from enum import Enum

class TeamsEnum(Enum):
    Arsenal = 1
    Bournemouth = 2
    Brighton = 3
    Burnley = 4
    Cardiff = 5
    Chelsea = 6
    Crystal_Palace = 7
    Everton = 8
    Fulham = 9
    Huddersfield = 10
    Leicester_City = 11
    Liverpool = 12
    Manchester_City = 13
    Manchester_United = 14
    Newcastle = 15
    Southampton = 16
    Spurs = 17
    Watford = 18
    West_Ham = 19
    Wolves = 20
